var Crawler = require("simplecrawler");

var crawler = new Crawler("http://michellerada.com");
crawler.interval = 500; // Ten seconds
crawler.maxConcurrency = 3;
crawler.on("fetchcomplete", function(queueItem, responseBuffer, response) {
    console.log("I just received %s (%d bytes)", queueItem.url, responseBuffer.length);
    console.log("It was a resource of type %s", response.headers['content-type']);
});
crawler.start();