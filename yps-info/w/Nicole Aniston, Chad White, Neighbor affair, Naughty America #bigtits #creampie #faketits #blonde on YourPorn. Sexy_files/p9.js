//---------------
var skip_btn_timer;
var ios_fs_timer;
var skip_btn_time = 8;
var progress_time = 10;
var ypsdwnld = false;
var vast_info = new Object();
vast_mode = false;
var YPSVnm = true;
var getYPSnmQ = false;
var timeoutid = 0;
var oldmode_trig = false;
var YPSVvol = 0.7;
//$(document).ready(function(){
function start_ypsp(){
	var duration = 0;
	var wrap_pid = "yps_player_wrap";
	var pb_md = false, pb_el;
	var ss_md = false, ss_el;	
	$("."+pid).wrap("<div class='yps_player_wrap'></div>");
	$(".yps_player_wrap").wrap("<div class='yps_player_wrap_wrap'></div>");
	var inside = "<div class='top-gradient noselect'></div>"; 
	inside+= "<div class='ypsv-body noselect' data-status='paused'><div class='ypsv-center'></div></div>";
	inside+= "<div class='bottom-gradient noselect'></div>";
	inside+= "<div class='ypsv-ctrl noselect' data-duration=0></div>";
	
	$(".yps_player_wrap").append(inside);
	ypsvb_start = "M 12,26 18.5,22 18.5,14 12,10 z M 18.5,22 25,18 25,18 18.5,14 z";
	ypsvb_pause = "M 12,26 16,26 16,10 12,10 z M 21,26 25,26 25,10 21,10 z";
	ypsvb_s0 = 'm 21.48,17.98 c 0,-1.77 -1.02,-3.29 -2.5,-4.03 v 2.21 l 2.45,2.45 c .03,-0.2 .05,-0.41 .05,-0.63 z m 2.5,0 c 0,.94 -0.2,1.82 -0.54,2.64 l 1.51,1.51 c .66,-1.24 1.03,-2.65 1.03,-4.15 0,-4.28 -2.99,-7.86 -7,-8.76 v 2.05 c 2.89,.86 5,3.54 5,6.71 z M 9.25,8.98 l -1.27,1.26 4.72,4.73 H 7.98 v 6 H 11.98 l 5,5 v -6.73 l 4.25,4.25 c -0.67,.52 -1.42,.93 -2.25,1.18 v 2.06 c 1.38,-0.31 2.63,-0.95 3.69,-1.81 l 2.04,2.05 1.27,-1.27 -9,-9 -7.72,-7.72 z m 7.72,.99 -2.09,2.08 2.09,2.09 V 9.98 z';	
	ypsvb_s1 = 'M8,21 L12,21 L17,26 L17,10 L12,15 L8,15 L8,21 Z M19,14 L19,22 C20.48,21.32 21.5,19.77 21.5,18 C21.5,16.26 20.48,14.74 19,14 ZM24,18 C 24,18 24,18 24,18 C 24,18 24,18 24,18 L24,18 C 24,18 24,18 24,18 C 24,18 24,18 24,18 L24,18 Z';
	ypsvb_s2 = 'M8,21 L12,21 L17,26 L17,10 L12,15 L8,15 L8,21 Z M19,14 L19,22 C20.48,21.32 21.5,19.77 21.5,18 C21.5,16.26 20.48,14.74 19,14 ZM19,11.29 C21.89,12.15 24,14.83 24,18 C24,21.17 21.89,23.85 19,24.71 L19,26.77 C23.01,25.86 26,22.28 26,18 C26,13.72 23.01,10.14 19,9.23 L19,11.29 Z';
	ypsvb_fs = '<svg height="100%" version="1.1" viewBox="0 5 36 24" width="100%" style="fill:#fff"><g class="ypsv-fullscreen-button-corner-0 ypsvs_transition"><path d="m 10,16 2,0 0,-4 4,0 0,-2 L 10,10 l 0,6 0,0 z"></path></g><g class="ypsv-fullscreen-button-corner-1 ypsvs_transition"><path d="m 20,10 0,2 4,0 0,4 2,0 L 26,10 l -6,0 0,0 z"></path></g><g class="ypsv-fullscreen-button-corner-2 ypsvs_transition"><path d="m 24,24 -4,0 0,2 L 26,26 l 0,-6 -2,0 0,4 0,0 z"></path></g><g class="ypsv-fullscreen-button-corner-3 ypsvs_transition"><path d="M 12,20 10,20 10,26 l 6,0 0,-2 -4,0 0,-4 0,0 z"></path></g></svg>';
	ypsvb_sf = '<svg height="100%" version="1.1" viewBox="0 0 36 36" width="100%" style="fill:#fff"><g class="ypsv-fullscreen-button-corner-0 ypsvs_transition"><path d="m 14,14 -4,0 0,2 6,0 0,-6 -2,0 0,4 0,0 z"></path></g><g class="ypsv-fullscreen-button-corner-1 ypsvs_transition"><path d="m 22,14 0,-4 -2,0 0,6 6,0 0,-2 -4,0 0,0 z"></path></g><g class="ypsv-fullscreen-button-corner-2 ypsvs_transition"><path d="m 20,26 2,0 0,-4 4,0 0,-2 -6,0 0,6 0,0 z"></path></g><g class="ypsv-fullscreen-button-corner-3 ypsvs_transition"><path d="m 10,22 4,0 0,4 2,0 0,-6 -6,0 0,2 0,0 z"></path></g></svg>';
	ypsvb_ss = '<svg height="100%" version="1.1" viewBox="0 5 36 24" width="100%"><path class="ypsv-ss-fill ypsvs_transition" d="'+ypsvb_start+'" fill="#FFF"></path></svg>';
	ypsvb_sd = '<svg height="100%" version="1.1" viewBox="0 5 36 24" width="100%"><path class="ypsv-vs-fill ypsvs_transition" d="'+ypsvb_s2+'" fill="#FFF"></path></svg>';
	yps_sslider = "<div class='ypsv-vol-slider' data-lvl='70'><div class='ypsv-vol-bar'><div class='ypsv-vol-done'></div><div class='ypsv-vol-cur'></div></div></div>";
	var ypsv_ctrl = "<div class='ypsv-pb-wrap'><div class='ypsv-pb-body ypsv_transition' data-curtime=0 data-duration=0 data-paused=1><div class='ypsv-pb-buff'></div><div class='ypsv-pb-sel'><div class='ypsv-pb-sel-time'></div></div><div class='ypsv-pb-ads-mark'></div><div class='ypsv-pb-done'></div><div class='ypsv-pb-cur'></div></div></div>";
	ypsv_ctrl+= "<div class='ypsv-ctrl-btns'><div class='ypsv-ss ypsv-start'>"+ypsvb_ss+"</div><div class='ypsv-vol-ctrl'><div class='ypsv-vol-spk'>"+ypsvb_sd+"</div>"+yps_sslider+"</div><div class='ypsv-time'><span class='ypsv-curtime'></span><i class='ypsv-curtime-i ypsv_before_loader'>|</i><span class='ypsv-duration'></span></div><div class='ypsv-fullscr'>"+ypsvb_fs+"</div><div class='ypsv-download'><a href='' download>D</a></div></div>";
//-------------------
	$(".ypsv-ctrl").append(ypsv_ctrl);
	$(".yps_player_wrap_wrap").append("<div class='ypsv-nm-div' style='min-height:20px;color:#fff;background-color:#345;width:100%;'><input class='ypsv-nm-chbx' type='checkbox' name='ypsv-nm-chbx' onclick='setYPSnm(this)' checked><span>Native Player Mode</span></div>");
	if (!supports_html5_storage() || !supports_html5_storage2()) {
		//$('.ypsv-nm-div span').html('Native Player Mode (!pm)');
		YPSVnm = true;
		$('.yps_player_wrap').find('.ypsv-ctrl, .ypsv-body, .bottom-gradient').css('display','none');
		$('.yps_player_wrap').find('video').attr('controls',true);
	} else {
		//$('.ypsv-nm-div span').html('Native Player Mode (pm)');
		YPSVnm = getYPSnm();
	}
	if (YPSVnm) {
		$('.ypsv-nm-chbx').attr('checked', true);
	} else {
		$('.ypsv-nm-chbx').removeAttr('checked');
		$("."+pid).removeAttr('controls').attr('playsinline','');
		var YPSVvol = getYPSVvol();
		$(".ypsv-vol-cur").css('left',(YPSVvol*100) +'%');
		$(".ypsv-vol-done").css('width',(YPSVvol*100) + '%');
		set_cur_vol(".ypsv-vol-slider",(YPSVvol*100));	
	}	
	//-------------------
	$(document).on('mousemove','html', function(e) {
		if (pb_md) {
			var duration = $(pb_el).find('.ypsv-pb-body').attr('data-duration');
			$(pb_el).find('.ypsv-pb-body').attr('data-curtime',get_sel_pos(pb_el,".ypsv-pb-body",e)*duration/100);
			$(pb_el).find(".ypsv-pb-cur").css('left',get_sel_pos(pb_el,".ypsv-pb-body",e)+'%');
			$(pb_el).find(".ypsv-pb-done").css('width',get_sel_pos(pb_el,".ypsv-pb-body",e)+'%');
		}
		if (ss_md) {
			var lvl = get_sel_pos(ss_el,".ypsv-vol-bar",e);
			$(ss_el).find(".ypsv-vol-cur").css('left',lvl+'%');
			$(ss_el).find(".ypsv-vol-done").css('width',lvl+'%');
			set_cur_vol(ss_el,lvl);
		}
	});
	$(document).on('mousemove','.ypsv-pb-wrap', function(e) {
		$(this).find(".ypsv-pb-sel").css('width',get_sel_pos(this,".ypsv-pb-body",e)+'%');
		var duration = $(this).find('.ypsv-pb-body').attr('data-duration');
		$(this).find(".ypsv-pb-sel-time").html(sec_conv(get_sel_pos(this,".ypsv-pb-body",e)*duration/100));
	});
	$(document).on('mouseout','.ypsv-pb-wrap', function(e) {
		$(this).find(".ypsv-pb-sel").css('width','0%');
	});
	$(document).on('mousedown','.ypsv-pb-wrap', function(e) {
		pb_el = this;
		pb_md = true;
			if ($(this).closest('.yps_player_wrap').find("video").get(0).paused) {
				$(this).find('.ypsv-pb-body').attr('data-paused',1);
			} else {
				$(this).find('.ypsv-pb-body').attr('data-paused',0);
			}
			$(this).closest('.yps_player_wrap').find("video").get(0).pause();
			var duration = $(this).find('.ypsv-pb-body').attr('data-duration');
			$(this).find('.ypsv-pb-body').attr('data-curtime',get_sel_pos(this,".ypsv-pb-body",e)*duration/100);			
			$(this).addClass('ypsv-onclickhold');
			$(this).find(".ypsv-pb-cur").css('left',get_sel_pos(this,".ypsv-pb-body",e)+'%');
			$(this).find(".ypsv-pb-done").css('width',get_sel_pos(this,".ypsv-pb-body",e)+'%');
	});
	$(document).on('mouseup','html', function(e) {
		$('.ypsv-pb-wrap').removeClass('ypsv-onclickhold');
		if (pb_md) {
			$(pb_el).closest('.yps_player_wrap').find("video").get(0).currentTime = $(pb_el).find('.ypsv-pb-body').attr('data-curtime');
			if ($(pb_el).find('.ypsv-pb-body').attr('data-paused') == 0) {
				$(pb_el).closest('.yps_player_wrap').find("video").get(0).play();
			}	
		}
		pb_md = false;
		ss_md = false;
	});
	$(document).on('mousedown','.ypsv-vol-slider', function(e) {
		ss_md = true;
		ss_el = this;
		var lvl = get_sel_pos(this,".ypsv-vol-bar",e);
			$(this).find(".ypsv-vol-cur").css('left',lvl+'%');
			$(this).find(".ypsv-vol-done").css('width',lvl+'%');
			set_cur_vol(this,lvl);
	});
	if (document.addEventListener) {
		document.addEventListener('webkitfullscreenchange', fsExitHandler, false);
		document.addEventListener('mozfullscreenchange', fsExitHandler, false);
		document.addEventListener('fullscreenchange', fsExitHandler, false);
	}
	function fsExitHandler() {
	if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement) {
			var el = $(this).find("."+pid).get(0);
			exitFS(el);
		}
	}
	//-------------------
	$("."+pid).each(function() {
		this.addEventListener('play', function() {
			if (!oldmode_trig) {
				console.log("video_player_play");
				oldmode_trig = true;
				var video_player_ended = function(event){
					console.log("video_player_ended");
					endVAST(this,'end');
					this.removeEventListener('ended', video_player_ended);
				}
				this.addEventListener('ended', video_player_ended);
				tryVAST(this, true);
			}
		});
		this.addEventListener('webkitendfullscreen', function() {
			if (!this.webkitDisplayingFullscreen) {
				exitFS(this);
			}
		});
		this.addEventListener('ended', function() {
			$(this).closest('.yps_player_wrap').find('.ypsv-ss-fill').attr("d",ypsvb_start);
			$(this).closest('.yps_player_wrap').find('.ypsv-pause').addClass('ypsv-start').removeClass('ypsv-pause');
			$(this).closest('.yps_player_wrap').find(".ypsv-body").attr('data-status','paused');
			if (!YPSVnm) exitFS(this);
			endVAST(this,'end');
		});
		this.addEventListener('waiting', function() {
			$(this).closest('.yps_player_wrap').find(".ypsv-center").addClass('ypsv_loader');
			$(this).closest('.yps_player_wrap').find(".ypsv-body").attr('data-status','buffering');
		});
		this.addEventListener('playing', function() {
			$(this).closest('.yps_player_wrap').find(".ypsv-body").attr("data-status","playing");
			$(this).closest('.yps_player_wrap').find('.ypsv-ss-fill').attr("d",ypsvb_pause);
			$(this).closest('.yps_player_wrap').find('.ypsv-start').addClass('ypsv-pause').removeClass('ypsv-start');
			$(this).closest('.yps_player_wrap').find(".ypsv-center").removeClass('ypsv_loader');
			$(this).closest('.yps_player_wrap').find(".ypsv-ctrl").attr('data-ch',1);
			timeoutid = setTimeout(hideYPSVctrl, 2000);
			if (vast_mode) {
				//skipButtonStart(this);
			}
		});
		this.addEventListener('pause', function() {
			$('.ypsv-ctrl[data-ch=1]').removeClass('ypsv-ctrl-hm');
			$(this).closest('.yps_player_wrap').find(".ypsv-body").attr("data-status","paused");
			$(this).closest('.yps_player_wrap').find('.ypsv-ss-fill').attr("d",ypsvb_start);
			$(this).closest('.yps_player_wrap').find('.ypsv-pause').addClass('ypsv-start').removeClass('ypsv-pause');
			$(this).closest('.yps_player_wrap').find(".ypsv-ctrl").attr('data-ch',0);
		});
		this.addEventListener('loadedmetadata', function() {
			$(this).closest('.yps_player_wrap').find(".ypsv-pb-cur").css('left','0%');
			$(this).closest('.yps_player_wrap').find(".ypsv-pb-done").css('width','0%');
			$(this).closest('.yps_player_wrap').find(".ypsv-curtime-i").removeClass('ypsv_before_loader');
			$(this).closest('.yps_player_wrap').find('.ypsv-curtime').html(sec_conv(this.currentTime));
			$(this).closest('.yps_player_wrap').find('.ypsv-duration').html(sec_conv(this.duration));
			$(this).closest('.yps_player_wrap').find('.ypsv-pb-body').attr('data-duration',this.duration);
			$(this).closest('.yps_player_wrap').find('.ypsv-pb-ads-mark').html("");
			if (!vast_mode) {
				for (var i=0;i<vast_time.length;i++) {
					if (vast_time[i] == -1) continue;
					var pos = vast_time[i]/this.duration*100;
					$(this).closest('.yps_player_wrap').find('.ypsv-pb-ads-mark').append("<div style='left:"+pos+"%'></div>");			
				}
			}
			if (!ypsdwnld) {
				$(this).closest('.yps_player_wrap').find('.ypsv-download').css('visibility','visible');
				$(this).closest('.yps_player_wrap').find('.ypsv-download > a').attr("href",this.src);
				ypsdwnld = true;
			}			
		});
		this.addEventListener('timeupdate', function() {
			tryVAST(this, false);
			if ($(this).closest('.yps_player_wrap').find("video").get(0).paused) return;
			var buff_end = 0;
			for(var b=this.buffered.length-1;0<=b;){
				if(this.buffered.start(b)<=this.currentTime) {
					buff_end = this.buffered.end(b)/this.duration*100;
					break;
				}	
				b--;
			}
			$(this).closest('.yps_player_wrap').find(".ypsv-pb-buff").css({'left':'0%','width':buff_end+'%'});
			$(this).closest('.yps_player_wrap').find('.ypsv-curtime').html(sec_conv(this.currentTime));
			var done = this.currentTime/this.duration*100;
			$(this).closest('.yps_player_wrap').find(".ypsv-pb-cur").css('left',done+'%');
			$(this).closest('.yps_player_wrap').find(".ypsv-pb-done").css('width',done+'%');
		});
	});
	$(".yps_player_wrap").mousemove(function() {
		if ($(this).find('.ypsv-ctrl').attr('data-ch') == 0 || vast_mode) return;
		if ($(this).find("video").get(0).paused) return;
		if (timeoutid) {
		   clearTimeout(timeoutid);
		   timeoutid = 0;
		}
		timeoutid = setTimeout(hideYPSVctrl, 2000);
		$('.ypsv-ctrl[data-ch=1]').removeClass('ypsv-ctrl-hm');
	});
	//-------------------
	$(document).on('click','.ypsv-download, .ypsv-start, .ypsv-body[data-status="paused"]', function(e) {
		$(this).closest('.yps_player_wrap').find("video").get(0).play();
	});
	$(document).on('click','.ypsv-pause, .ypsv-body[data-status="playing"]', function(e) {
		$(this).closest('.yps_player_wrap').find("video").get(0).pause();
	});	
	$(document).on('click','.ypsv-vol-spk', function(e) {
		if ($(this).closest('.yps_player_wrap').find("video").get(0).muted) {
			var lvl = $(this).closest('.yps_player_wrap').find("video").get(0).volume*100;
			$(this).closest('.ypsv-vol-ctrl').find(".ypsv-vol-cur").css('left',lvl+'%');
			$(this).closest('.ypsv-vol-ctrl').find(".ypsv-vol-done").css('width',lvl+'%');
			set_cur_vol(this,lvl);
			$(this).closest('.yps_player_wrap').find("video").get(0).muted = false;
		} else {
			$(this).closest('.ypsv-vol-ctrl').find(".ypsv-vol-cur").css('left',0+'%');
			$(this).closest('.ypsv-vol-ctrl').find(".ypsv-vol-done").css('width',0+'%');
			$(this).closest('.ypsv-vol-ctrl').find('.ypsv-vs-fill').css('opacity','0.8').attr("d",ypsvb_s0);
			$(this).closest('.yps_player_wrap').find("video").get(0).muted = true;
		}
	});	
	$(document).on('click','.ypsv-fullscr', function(e) {
		if ($(this).closest('.yps_player_wrap').hasClass('yps_player_fs')) {
			exitFS(this);
		} else {
			goToFS(this);
		}
	});
	//VAST
	$.each(vast_urls, function (key, value){getVastInfo(key, value)});
}
//});
function goToFS(el){
	var pel = $(el).closest('.yps_player_wrap').get(0);
	var vid = $(el).closest('.yps_player_wrap').find('video').get(0);
	if (pel.requestFullscreen) {
		pel.requestFullscreen();
	} else if (pel.msRequestFullscreen) {
		pel.msRequestFullscreen();
	} else if (pel.mozRequestFullScreen) {
		pel.mozRequestFullScreen();
	} else if (pel.webkitRequestFullscreen) {
		pel.webkitRequestFullscreen();
	} else if (vid.webkitEnterFullScreen) {
		vid.webkitEnterFullScreen();
	}
	$(el).closest('.yps_player_wrap').addClass('yps_player_fs');
	$(el).closest('.yps_player_wrap').find('.ypsv-fullscr').html(ypsvb_sf);
	/*
	ios_fs_timer = setInterval(function() {
		$("."+pid).each(function() {
			if (!this.webkitDisplayingFullscreen) {
				exitFS(this);
				clearInterval(ios_fs_timer);
			}	
		});
	},200);
	*/
}
function skipButtonStart(el){
	$(el).closest('.yps_player_wrap').find(".ypsv-vast-skip").attr('data-curtime',skip_btn_time).attr('data-status',2).html('Skip Ad in '+skip_btn_time);
	skip_btn_timer = setInterval(function() {
		var lasttime = $(".ypsv-vast-skip[data-status=2]").attr('data-curtime');
		var curtime = Math.floor(lasttime-1);
		console.log('curtime',curtime);
		if (curtime <= 0) {
			$(".ypsv-vast-skip[data-status=2]").html('Skip Ad').attr('data-status',1).attr('data-curtime',0);
			clearInterval(skip_btn_timer);
		} else {
			$(".ypsv-vast-skip[data-status=2]").attr('data-curtime',curtime).html('Skip Ad in '+curtime);	
		}
	},1000);
}
function exitFS(el){
	var vid = $(el).closest('.yps_player_wrap').find('video').get(0);
	if (document.exitFullscreen) {
		document.exitFullscreen();
	} else if (document.msExitFullscreen) {
		document.msExitFullscreen();
	} else if (document.mozCancelFullScreen) {
		document.mozCancelFullScreen();
	} else if (document.webkitExitFullscreen) {
		document.webkitExitFullscreen();
	} else if (vid.webkitExitFullscreen) {
		vid.webkitExitFullscreen();
	}
	$(el).closest('.yps_player_wrap').removeClass('yps_player_fs');
	$(el).closest('.yps_player_wrap').find('.ypsv-fullscr').html(ypsvb_fs);
}
function endVAST(el,mode){
	if (vast_mode) {
		var key = Object.keys(vast_info)[0];
		/*
		if (conf_on_end && mode == 'end' && !YPSVnm) {
			$(el).closest('.yps_player_wrap').find('.ypsv-vast-skip').html('Continue Watching >');
			return;
		}
		*/
		$(el).closest('.yps_player_wrap').find('.ypsv-vast-body').remove();
		el.removeEventListener('timeupdate', yh5vTracking);
		delete vast_info[key];
		el.src = $(el).attr('data-puri');
		el.load();
		el.play();
		$(el).removeAttr('data-puri');
		if (YPSVnm) $(el).closest('.yps_player_wrap').find('video').attr('controls',true);
		vast_mode = false;
	}
};
function tryVAST(el, oldmode){
	var vast_length = Object.keys(vast_info).length;
	if (vast_length == 0 || vast_mode) return;
	var key = Object.keys(vast_info)[0];
	var time = vast_time[0];
	if ((el.currentTime > time && time != -1) || oldmode) {
		vast_mode = true;
		if (vast_info[key].hasOwnProperty('Impression')) {
			vast_info[key].Impression.forEach(function(el) {
				yh5vTrack(el.trim());
			});
		}
		if (vast_info[key].hasOwnProperty('MediaFile') && vast_info[key].hasOwnProperty('ClickThrough')) {
			//el.pause();
			vast_time.shift();
			var prev_src = el.src;
			var regex = /#t=(\d+)/;
			var patt = new RegExp(regex);
			if (patt.test(prev_src)) {
				prev_src = prev_src.replace(regex, "#t=" + Math.floor(el.currentTime));
			} else {
				prev_src+= "#t=" + Math.floor(el.currentTime);
			}
			$(el).attr('data-puri',prev_src);
			$(el).closest('.yps_player_wrap').find(".ypsv-ctrl").attr('data-ch',1);
			hideYPSVctrl();
			$(el).closest('.yps_player_wrap').append("<div class='ypsv-vast-body noselect'><div class='ypsv-vast-skip noselect' data-status=0></div></div>");
			$(el).closest('.yps_player_wrap').find(".ypsv-curtime-i").addClass('ypsv_before_loader');
			if (YPSVnm) $(el).closest('.yps_player_wrap').find('video').removeAttr('controls');
			if (!YPSVnm) exitFS(el);
			skipButtonStart(el);
			var mfmp4 = vast_info[key].MediaFile.trim();
			if (mfmp4.indexOf(".mp4") != -1) {
				el.src = mfmp4;
				el.load();
				el.play();
			}
			$(el).closest('.yps_player_wrap').find('.ypsv-vast-body').css('display','block').click(function() {
				window.open(vast_info[key].ClickThrough.trim());
				if (vast_info[key].hasOwnProperty('ClickTracking')) {
					vast_info[key].ClickTracking.forEach(function(el) {
						yh5vTrack(el.trim());
					});
				}
			});
			$(el).closest('.yps_player_wrap').find('.ypsv-vast-skip').attr('data-key',key);
			$(el).closest('.yps_player_wrap').find('.ypsv-vast-skip').click(function(e) {
				e.stopPropagation();
				var key = $(this).attr('data-key');
				if (vast_info[key].hasOwnProperty('TrackingSkip')) {
					vast_info[key].TrackingSkip.forEach(function(el) {
						yh5vTrack(el.trim());
					});	
				}
				if ($(this).attr('data-status') == 1) {
					endVAST(el,'skip');
				}
			});
			if (vast_info[key].hasOwnProperty('TrackingProgress')) {
				el.addEventListener('timeupdate', yh5vTracking);
			}
			if (vast_info[key].hasOwnProperty('TrackingStart')) {
				vast_info[key].TrackingStart.forEach(function(el) {
					yh5vTrack(el.trim());
				});	
			}
			//el.addEventListener('timeupdate', skipBtnUpd);
		} else {
			vast_mode = false;
			delete vast_info[key];
			tryVAST(el,true);
		}
	}
}
function yh5vTracking() {
	if (this.currentTime>progress_time) {
		var key = Object.keys(vast_info)[0];
		vast_info[key].TrackingProgress.forEach(function(el) {
			yh5vTrack(el.trim());
		});
		this.removeEventListener('timeupdate', yh5vTracking);
	}
}
/*
function skipBtnUpd() {
	var skip_btn_time_left = skip_btn_time - Math.round(this.currentTime); console.log('timupd',skip_btn_time_left);
	if (skip_btn_time_left <= 0) {
		$(this).closest('.yps_player_wrap').find('.ypsv-vast-skip').html('Skip Ad').attr('data-status',1);
	} else {
		$(this).closest('.yps_player_wrap').find('.ypsv-vast-skip').html('Skip Ad in '+skip_btn_time_left);					
	}
};
*/
function yh5vTrack(url){
	var img_track = new Image();
	img_track.src = url;
}
function yh5vAddPixel(pixel_url){
	var image = new Image(1,1); 
	image.src = pixel_url;
}
function getVastInfo(key, xmlurl){
	if (!vast_info.hasOwnProperty(key)) vast_info[key] = new Object();
	$.ajax({
		type: "GET",
		url: xmlurl,
		dataType: "xml",
		success: function(xml){
			$(xml).find('MediaFile').each(function(){
				var MF = $(this).text();
				if (MF.indexOf(".mp4") > -1) {
					vast_info[key].MediaFile = MF;
				}
			});
			$(xml).find('ClickThrough').each(function(){
				if ($(this).text()) vast_info[key].ClickThrough = $(this).text();
			});
			$(xml).find('ClickTracking').each(function(){
				if (!vast_info[key].hasOwnProperty('ClickTracking')) vast_info[key].ClickTracking = [];
				if ($(this).text()) vast_info[key].ClickTracking.push($(this).text());
			});			
			$(xml).find('Duration').each(function(){
				if ($(this).text()) vast_info[key].Duration = $(this).text().trim();
			});
			$(xml).find('Tracking').each(function(){
				if ($(this).attr('event') == 'start') { 
					if (!vast_info[key].hasOwnProperty('TrackingStart')) vast_info[key].TrackingStart = [];
					if ($(this).text()) vast_info[key].TrackingStart.push($(this).text());
				}
				if ($(this).attr('event') == 'skip') { 
					if (!vast_info[key].hasOwnProperty('TrackingSkip')) vast_info[key].TrackingSkip = [];
					if ($(this).text()) vast_info[key].TrackingSkip.push($(this).text());
				}
				if ($(this).attr('event') == 'progress') { 
					if (!vast_info[key].hasOwnProperty('TrackingProgress')) vast_info[key].TrackingProgress = [];
					if ($(this).text()) vast_info[key].TrackingProgress.push($(this).text());
				}				
			});
			$(xml).find('Impression').each(function(){
				if (!vast_info[key].hasOwnProperty('Impression')) vast_info[key].Impression = [];
				if ($(this).text()) vast_info[key].Impression.push($(this).text());
			});
			$(xml).find('VASTAdTagURI').each(function(){
				console.log($(this).text());
				/* event
				$.post("/player/tr.php", {v: $(this).text()}, function(result){
					if (result != "error") {
					}
				});
				*/
				getVastInfo(key, $(this).text());
			});
			//console.log('lllllllllll',vast_info);
		},
		error: function() {
			console.log("ERROR_GET:",key);
		}
	});
}
function hideYPSVctrl(){
	var isHovered = $('.ypsv-ctrl[data-ch=1]').filter(function() {
        return $(this).is(":hover");
    });
	if (isHovered.length > 0 && !vast_mode) {
		if (timeoutid) {
		   clearTimeout(timeoutid);
		   timeoutid = 0;
		}
		timeoutid = setTimeout(hideYPSVctrl, 2000);
		return;
	}
	$('.ypsv-ctrl[data-ch=1]').addClass('ypsv-ctrl-hm');
}
function set_cur_vol(el,lvl){
	lvl = Math.floor(lvl);
	dlvl = lvl/100;
	var fixed = dlvl.toFixed(1);
	$(el).attr('data-lvl',fixed);
	$(el).closest('.yps_player_wrap').find("video").get(0).volume = fixed;
	setYPSVvol(fixed);
	if (lvl == 0) {
		$(el).closest('.ypsv-vol-ctrl').find('.ypsv-vs-fill').css('opacity','0.8').attr("d",ypsvb_s0);
	} else if (lvl > 0 && lvl < 50) {
		$(el).closest('.ypsv-vol-ctrl').find('.ypsv-vs-fill').css('opacity','1').attr("d",ypsvb_s1);
	} else if (lvl >= 50) {
		$(el).closest('.ypsv-vol-ctrl').find('.ypsv-vs-fill').css('opacity','1').attr("d",ypsvb_s2);
	}
}
function sec_conv(secs){
	var seconds = Math.floor(secs),
	hours = Math.floor(seconds / 3600);
    seconds -= hours*3600;
    var minutes = Math.floor(seconds / 60);
    seconds -= minutes*60;

	if (hours == 0) {hours = "";}
	if (hours > 0 && hours < 10) {hours = "0"+hours+':';}
	if (hours > 10) {hours = hours+':';}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+minutes+":"+seconds;
}
function canplaythrough(el){
	var node = $(el).get(0);
	vid = document.getElementById("yps_player");
}
function get_sel_pos(el,base,e){
	var pos = e.pageX - $(el).find(base).offset().left;
	if (pos < 0) pos = 0;
	if (pos > $(el).find(base).width()) pos = $(el).find(base).width();
	return (pos / $(el).find(base).width()) * 100;
}
function supports_html5_storage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
} catch (e) {
    return false;
  }
}
function supports_html5_storage2() {
  try {
    var storage = window.sessionStorage;
	storage.setItem("YPSVnm5test", "test");
    storage.removeItem("YPSVnm5test");
	return true;
} catch (e) {
    return false;
  }
}
function setYPSVvol(lvl) {
	if (!supports_html5_storage()) return false;
	localStorage.setItem('YPSVvol',lvl);
}
function getYPSVvol() {
	if (!supports_html5_storage()) return 0.7;
	if (localStorage.getItem('YPSVvol') !== null) {
		return localStorage.getItem('YPSVvol');
	} else {
		localStorage.setItem('YPSVvol',0.7);
		return 0.7;
	}
}
function setYPSnm(nm) {
	if (!supports_html5_storage()) return false;
	if (nm.checked) localStorage.setItem('YPSVnm5',1); else localStorage.setItem('YPSVnm5',0);
	getYPSnm();
}
function getYPSnm() {
	if (!supports_html5_storage()) {
		//try
	} else {
		if (localStorage.getItem('YPSVnm5') !== null) {
			if (localStorage.getItem('YPSVnm5') == 1 || getYPSnmQ) {
				$('.yps_player_wrap').find('.ypsv-ctrl, .ypsv-body, .bottom-gradient').css('display','none');
				$('.yps_player_wrap').find('video').attr('controls',true);
				return true;
			} else {
				$('.yps_player_wrap').find('.ypsv-ctrl, .ypsv-body, .bottom-gradient').css('display','block');
				$('.yps_player_wrap').find('video').removeAttr('controls');
				return false;
			}	
		} else {
			$('.yps_player_wrap').find('.ypsv-ctrl, .ypsv-body, .bottom-gradient').css('display','none');
			$('.yps_player_wrap').find('video').attr('controls',true);
			localStorage.setItem('YPSVnm5',1);
			return true;
		}
	}
}