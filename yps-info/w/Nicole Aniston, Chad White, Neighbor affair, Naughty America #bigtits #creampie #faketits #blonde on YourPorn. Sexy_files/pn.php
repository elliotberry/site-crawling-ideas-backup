/* global pn_idzone, pn_soft_ask, pn_soft_ask_horizontal_position, pn_soft_ask_vertical_position, pn_soft_ask_title, pn_soft_ask_description,
pn_soft_ask_yes, pn_soft_ask_no, pn_soft_ask_title_enabled */

var exoLaunchPN = function(event) {
    if (typeof pn_idzone === 'undefined') {
        console.log("pn_idzone not defined");

        return;
    }

    var idZone = parseInt(pn_idzone);

    if (typeof pn_filename === 'undefined') {
        pn_filename = 'worker.js';
    }

    if (typeof pn_sleep_seconds === 'undefined') {
        pn_sleep_seconds = 5;
    }

    if (typeof pn_is_self_hosted === 'undefined') {
        pn_is_self_hosted = 0;
    }

    var cookieNameZoneSoftAsk = 'pn-zone-ask';
    var cookieNameZoneSubscribe = 'pn-zone-done';
    var softAskOnceInHours = 48;
    var checkSubscriptionAfterDays = 45;

    var pn_soft_ask_translations = {
        en: {
            title: "Click ALLOW to continue",
            description: "Would you like to receive great special offers and promotions?",
            yes: "Allow",
            no: "No, Thanks"
        },
        es: {
            title: "Haz clic en PERMITIR para continuar",
            description: "¿Te gustaría recibir ofertas especiales y promociones?",
            yes: "Permitir",
            no: "No, gracias"
        },
        fr: {
            title: "Cliquez sur AUTORISER pour continuer",
            description: "Souhaitez-vous recevoir d'excellentes offres spéciales et promotions?",
            yes: "Autoriser",
            no: "Non, merci"
        },
        de: {
            title: "Klicken Sie ERLAUBEN um fortzufahren",
            description: "Möchten Sie tolle Sonderangebote und Aktionen erhalten?",
            yes: "Erlauben",
            no: "Nein, Danke"
        },
        it: {
            title: "Clicca su \"ACCETTA\" per continuare",
            description: "Ti piacerebbe ricevere imperdibili offerte e promozioni?",
            yes: "Accetta",
            no: "No, grazie"
        },
        zh: {
            title: "单击“允许”继续",
            description: "你想获得特别优惠和促销活动吗？",
            yes: "允许",
            no: "不，谢谢"
        },
        pt: {
            title: "Clique em \"Permitir\" para continuar",
            description: "Gostaria de receber ofertas especiais e promoções?",
            yes: "Permitir",
            no: "Não, obrigado"
        },
        ru: {
            title: "Нажмите РАЗРЕШИТЬ, чтобы продолжить",
            description: "Вы бы хотели получать информацию о специальных предложениях и промо-акциях?",
            yes: "Разрешить",
            no: "Нет, спасибо"
        },
        cs: {
            title: "Klikni na Ano",
            description: "Chcete dostávat naše speciální nabídky a promo akce?",
            yes: "Ano",
            no: "Ne, děkuji"
        },
        sk: {
            title: "Pokračujte kliknutím na POVOLIŤ",
            description: "Máte záujem o zasielanie skvelých špeciálnych ponúk a akcií?",
            yes: "Áno",
            no: "Nie, ďakujem"
        },
        hu: {
            title: "Kattintson az Engedelyézem linkre a folytatáshoz",
            description: "Szeretnél értesülni különleges ajánlatokról és akciókról?",
            yes: "Engedélyezem",
            no: "Nem, Köszönöm"
        },
        hr: {
            title: "Kliknite DOPUSTI za nastavak",
            description: "Da li zelite primati specijale ponude i promocije",
            yes: "Dopusti",
            no: "Ne, hvala",
        }

    };

    if (typeof pn_soft_ask === 'undefined' || pn_soft_ask == 1) {
        setTimeout(runPnSoftAsk,  pn_sleep_seconds * 1000);
    } else if (typeof pn_is_self_hosted !== 'undefined' && pn_is_self_hosted == 1) {
        setTimeout(runSubscription, pn_sleep_seconds * 1000);
    } else {
        console.log("Wrong combination of pn_soft_ask or pn_is_self_hosted");
    }


    function runPnSoftAsk() {
        if (isZoneInCookieSoftAsk(idZone) || isZoneInCookieSoftAsk(idZone)) {
            return;
        }

        var text = getText();

        var elem = document.createElement('div');
        elem.id = 'wpnAskModalContainer';
        elem.style.display = 'none';

        elem.innerHTML =
            getCss() +
            '<div id="wpnAskModal">\n' +
                '<div id="wpnAskInner">\n' +
                    '<div id="wpnAskDescription">' + text.description + '</div>\n' +
                    '<div id="wpnAskTitle">' + text.title + '</div>\n' +
                    '<div id="wpnAskBtnNo">' + text.no + '</div>\n' +
                    '<div id="wpnAskBtnYes">' + text.yes + '</div>' +
                '</div>\n' +
            '</div>';

        document.body.appendChild(elem);

        elem.style.display = 'flex';

        document.getElementById('wpnAskBtnYes').onclick = function() {
            addZoneIntoCookieSoftAsk(idZone);
            elem.style.display = 'none';

            runSubscription();
        };

        document.getElementById('wpnAskBtnNo').onclick = function() {
            elem.style.display = 'none';
            addZoneIntoCookieSoftAsk(idZone);
        };
    }

    function getText() {
        var translatedText = {};

        var hasCustomTitle = (typeof pn_soft_ask_title !== 'undefined');
        var hasCustomDescription = (typeof pn_soft_ask_description !== 'undefined');
        var hasCustomYes = (typeof pn_soft_ask_yes !== 'undefined');
        var hasCustomNo = (typeof pn_soft_ask_no !== 'undefined');

        if (!hasCustomTitle || !hasCustomDescription || !hasCustomYes || !hasCustomYes) {
            translatedText = getTranslatedText();
        }

        if  (hasCustomTitle) {
            translatedText.title = pn_soft_ask_title;
        }

        if (hasCustomDescription) {
            translatedText.description = pn_soft_ask_description;
        }

        if (hasCustomYes) {
            translatedText.yes = pn_soft_ask_yes;
        }

        if (hasCustomNo) {
            translatedText.no = pn_soft_ask_no;
        }

        return translatedText;
    }

    function getCss() {
        var alignItems = '';
        var justifyContent = '';

        switch (pn_soft_ask_horizontal_position) {
            case 'left':
                alignItems = 'flex-start';
                break;

            case 'right':
                alignItems = 'flex-end';
                break;

            default:
                alignItems = 'center';
                break;
        }

        switch (pn_soft_ask_vertical_position) {
            case 'top':
                justifyContent = 'flex-start';
                break;

            case 'bottom':
                justifyContent = 'flex-end';
                break;

            default:
                justifyContent = 'center';
                break;
        }

        var cssSoftAskTitle;

        if ((typeof pn_soft_ask_title_enabled === 'undefined') || (pn_soft_ask_title_enabled == 1)) {
            cssSoftAskTitle = 'font-size: 24px;\n\
                margin-bottom: 10px;'
            ;

        } else {
            cssSoftAskTitle = 'display: none;';
        }

        return '<style>\n\
            #wpnAskModalContainer::before,\n\
            #wpnAskModalContainer::after,\n\
            #wpnAskModalContainer div::before,\n\
            #wpnAskModalContainer div::after,\n\
            #wpnAskModalContainer div {\n\
                box-sizing: content-box;\n\
            }\n\
            \n\
            #wpnAskModalContainer {\n\
                all: initial;\n\
                box-sizing: content-box;\n\
                width: 100%;\n\
                height: 100%;\n\
                left: 0;\n\
                top: 0;\n\
                display: flex;\n\
                justify-content: ' + justifyContent + ';\n\
                position: absolute;\n\
                flex-direction: column;\n\
                align-items: ' + alignItems + ';\n\
            }\n\
            \n\
            #wpnAskModalContainer #wpnAskModal {\n\
                min-width: 350px;\n\
                max-width: 400px;\n\
                background-color: #ffffff;\n\
                box-shadow: 0px 4px 8px 0px rgba(0, 0, 0, 0.25);\n\
                border-radius: 4px;\n\
                font-family: Helvetica, Arial;\n\
                font-weight: normal;\n\
                color: #666666;\n\
                text-align: center;\n\
                padding: 10px;\n\
                margin: 10px;\n\
                position: absolute;\n\
                z-index: 99999;\n\
            }\n\
            \n\
            #wpnAskModalContainer #wpnAskModal #wpnAskInner #wpnAskDescription {\n\
                font-size: 16px;\n\
                margin-bottom: 10px;\n\
            }\n\
            \n\
            #wpnAskModalContainer #wpnAskModal #wpnAskInner #wpnAskTitle {\n\
                ' + cssSoftAskTitle + '\n\
            }\n\
            \n\
            #wpnAskModalContainer #wpnAskModal #wpnAskInner #wpnAskBtnYes,\n\
            #wpnAskModalContainer #wpnAskModal #wpnAskInner #wpnAskBtnNo {\n\
                width: 170px;\n\
                height: 40px;\n\
                border-radius: 2px;\n\
                text-transform: uppercase;\n\
                line-height: 40px;\n\
                display: block;\n\
                cursor: pointer;\n\
                font-size: 16px;\n\
            }\n\
            \n\
            #wpnAskModalContainer #wpnAskModal #wpnAskInner #wpnAskBtnYes {\n\
                color: #ffffff;\n\
                background-color: #4484f4;\n\
                float: right;\n\
            }\n\
            \n\
            #wpnAskModalContainer #wpnAskModal #wpnAskInner #wpnAskBtnNo {\n\
                color: #a8a8a8;\n\
                background-color: #fafafa;\n\
                border: 1px solid #e6e6e6;\n\
                float: left;\n\
            }\n\
            \n\
            @media\n\
                only screen and (max-width: 389px)\n\
            {\n\
                #wpnAskModalContainer #wpnAskModal {\n\
                    min-width: 90vw;\n\
                    max-width: 90vw;\n\
                    width: 90vw;\n\
                    padding: 2.5vw;\n\
                    margin: 2.5vw;\n\
                    border-radius: 1vw;\n\
                }\n\
                \n\
                #wpnAskModalContainer #wpnAskModal #wpnAskInner #wpnAskDescription {\n\
                    font-size: 3.5vw;\n\
                    margin-bottom: 2vw;\n\
                }\n\
                \n\
                #wpnAskModalContainer #wpnAskModal #wpnAskInner #wpnAskTitle {\n\
                    font-size: 5vw;\n\
                    margin-bottom: 2vw;\n\
                }\n\
                \n\
                #wpnAskModalContainer #wpnAskModal #wpnAskInner #wpnAskBtnYes,\n\
                #wpnAskModalContainer #wpnAskModal #wpnAskInner #wpnAskBtnNo {\n\
                    font-size: 4.5vw;\n\
                    width: 42vw;\n\
                    height: 10vw;\n\
                    line-height: 10vw;\n\
                    border-radius: 0.5vw;\n\
                }\n\
            }\n\
        </style>';
    }

    function getTranslatedText() {
        var language = 'en'; // default fallback

        try {
            if (Array.isArray(navigator.languages)) {
                for (var i = 0; i < navigator.languages.length; i++) {
                    var currentLanguage = navigator.languages[i];

                    if (currentLanguage && currentLanguage.length && pn_soft_ask_translations.hasOwnProperty(currentLanguage.substr(0, 2))) {
                        return pn_soft_ask_translations[currentLanguage.substr(0, 2)];
                    }
                }
            }

            if (pn_soft_ask_translations.hasOwnProperty(navigator.language.substr(0, 2))) {
                return pn_soft_ask_translations[navigator.language.substr(0, 2)];
            }
        } catch (e) {
            // will use default en
        }

        return pn_soft_ask_translations[language];
    }

    function getZonesFromCookie(name) {
        var re = new RegExp('(?:(?:^|.*;\\s*)' + name + '\\s*=\\s*([^;]*).*$)|^.*$');
        var zones = document.cookie.replace(re, "$1").split("|");

        return cleanZoneIdsArray(zones)
    }

    function isZoneInCookieSoftAsk(idZone) {
        return (getZonesFromCookie(cookieNameZoneSoftAsk).indexOf(idZone) > -1);
    }

    function isZoneInCookieSubscribe(idZone) {
        return (getZonesFromCookie(cookieNameZoneSubscribe).indexOf(idZone) > -1);
    }

    function addZoneIntoCookieSoftAsk(idZone) {
        var zonesSave = getZonesFromCookie(cookieNameZoneSoftAsk);
        zonesSave.push(idZone);
        document.cookie = cookieNameZoneSoftAsk + "=|" + zonesSave.join("|") + "|; max-age=" + (60 * 60 * softAskOnceInHours);
    }

    function addZoneIntoCookieSubscribe(idZone) {
        var zonesSave = getZonesFromCookie(cookieNameZoneSubscribe);
        zonesSave.push(idZone);

        document.cookie = cookieNameZoneSubscribe + "=|" + zonesSave.join("|") + "|; max-age=" + (60 * 60 * 24 * checkSubscriptionAfterDays);
    }

    function cleanZoneIdsArray(zones) {
        return zones
            .map(function (value) {
                return parseInt(value);
            })
            .filter(function (value, index, self) {
                return self.indexOf(value) === index;
            });
    }

    function openPnWindow(idZone) {
        var width = 512;
        var height = 385;

        // Vivaldi seems to consider height as outerHeight
        if (navigator.userAgent.indexOf('Vivaldi') !== -1) {
            height += 60;
        }

        var posHorizontal = (screen.width - width) / 2;
        var posVertical = (screen.height - height) / 2;

        window.open(
            "https://" + idZone + ".notifysrv.com",
            "pn_ask_prompt",
            "menubar = no, location = yes, resizable = no, scrollbars = no, status = no, height = " +
                height + ", width = " + width + ", left = " + posHorizontal + ", top = " + posVertical
        );
    }

    function loadJs(url, onLoadFunction) {
        var scriptTag = document.createElement('script');
        scriptTag.src = url;

        scriptTag.onload = onLoadFunction;
        scriptTag.onreadystatechange = onLoadFunction;

        document.body.appendChild(scriptTag);
    }

    function runSubscriber() {
        var subscriber = new exoPnSubscriber();

        subscriber.run(idZone, pn_filename);
    }

    function runSubscription() {
        if (pn_is_self_hosted) {
            loadJs('https://js.wpncdn.com/pn-subs.php', runSubscriber);
        } else {
            openPnWindow(idZone);
        }

        addZoneIntoCookieSubscribe(idZone);
    }

};

if (typeof pn_run_immediate === 'undefined') {
    pn_run_immediate = false;
}
if (pn_run_immediate) {
    exoLaunchPN();
} else {
    document.addEventListener("DOMContentLoaded", exoLaunchPN);
}
